rows = open("/Users/aditilalla/programming-with-python-2022/aoc/day04.txt", "r").read().splitlines()
counter_1=0
counter_2=0
for row in rows:
    #splitting each row into both elves individual section ranges
    range_a, range_b = row.split(",")
    #isolating the start and end of each elf's range
    a1, a2 = [int (x) for x in range_a.split("-")]
    b1, b2 = [int (x) for x in range_b.split("-")]

    #checking if one range is fully contained in the other; Part 1
    if (a1<=b1 and a2>=b2) or (b1<=a1 and b2>=a2):
        counter_1+=1
    
    #checking for any overlaps in range; Part 2
    if (b2>=a1>=b1) or (a2>=b1>=a1):
        counter_2+=1

print ("The answer for part 1 is:", counter_1)
print ("The answer for part 2 is:", counter_2)

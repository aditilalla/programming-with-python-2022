elf = 0
array=[]

file=open("/Users/aditilalla/programming-with-python-2022/aoc/day01.txt")
for line in file:
	line = line.strip()
	if line == "":
		array.append(elf)
		elf = 0
		continue
	elf += int(line)

array.append(elf)
array.sort(reverse=True)

total=0
for i in range (0,3):
	print(array[i])
	total += array[i]

print('The total calories of the top elf is =',array[0])
print('The total calories of the top 3 elves is =', total)

priority=0
file=open("/Users/aditilalla/programming-with-python-2022/aoc/day03.txt")
for line in file:
    midpoint = int(len(line)/2)
    set_of_common_items = set(line[0:midpoint]).intersection(line[midpoint:len(line)])
    common_item = set_of_common_items.pop()
    if (common_item.islower()==True):
        priority+=ord(common_item) - 96
    else:
        priority+=ord(common_item) - 38

print("The part 1 solution is:", priority)
#Part 2
priority=0
puzzle_input=open("/Users/aditilalla/programming-with-python-2022/aoc/day03.txt", "r").read().splitlines()

for i in range (0, len(puzzle_input), 3):
    elf_A=puzzle_input[i]
    elf_B=puzzle_input[i+1]
    elf_C=puzzle_input[i+2]
    badges=set(elf_A).intersection(elf_B)
    badges=set(badges).intersection(elf_C)
    badge=badges.pop()
    if (badge.islower()==True):
        priority+=ord(badge) - 96
    else:
        priority+=ord(badge) - 38

print("The part 2 solution is:", priority)
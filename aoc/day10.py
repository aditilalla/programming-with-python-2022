file=open("/Users/aditilalla/programming-with-python-2022/aoc/day10.txt").read().splitlines()
value=1
cpu=1
cycles=0
total=0
for i in range(len(file)):
    j=file[i].split()
    if file[i].startswith("n"):
        cycles+=1
        signal_strength=cycles*value
    elif file[i].startswith("a"):
        value=int(j[1])
        cycles+=1
        if cycles in (20,60,100,140,180,220):
            total+=(cycles*cpu)
        cycles+=1
        if cycles in (20,60,100,140,180,220):
            total+=(cycles*cpu)
        cpu+=value

if cycles in (20,60,100,140,180,220):
    total+=(cycles*cpu)

print("The part 1 solution is:", total)

#PART 2
value=1
cycle=0
output=""
middle=True
for i in range (len(file)):
    j=file[i].split()
    if value <= cycle%40 <= value+2:
        output+="#"
    else:
        output+="."
    if file[i].startswith("n"):
        cycle+=1
    elif file[i].startswith("a"):
        if not middle: 
            value=int(j[1])
        middle=not middle
        cycle+=2

for position, pixel in enumerate(output,1):
    if position%40==0:
        print(pixel, end="")
    else:
        print(pixel)
    
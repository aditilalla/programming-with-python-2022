file=open("/Users/aditilalla/programming-with-python-2022/aoc/day08.txt")
trees=file.read().splitlines()
#Calculating total number of trees around the edge 
edge = len(trees)*2 + (len(trees[0])-2)*2
total=0
for i, row in enumerate(trees):
    if i==0 or i==len(trees)-1:
        continue #Ignores all the trees in top and bottow row since they are already counted in the edge
    for j, height in enumerate(row):
        if j==0 or j==len(row)-1:
            continue #Ignores all the trees at the beginning or end of each row since they are already counted in the edge 
        height=int(height)
        left = all (int(x)<height for x in row[:j])
        right = all (int(x)<height for x in row[j+1:])
        top_rows=[row[j] for row in trees[:i]]
        bottom_rows=[row[j] for row in trees[i+1:]]
        top = all (int(x)<height for x in top_rows)
        bottom = all (int(x)<height for x in bottom_rows)
        total += any ([left, right, top, bottom])
print("The part 1 solution is:", total+edge)

#Part 2
tree_ints=[[int(x) for x in i] for i in trees]
highest_score=0
for i in range (len(tree_ints)):
    for j in range (len(tree_ints[0])):
        vision=1 #Multiplication by 0 would nullify all scores
        temp=0
        for k in range (i+1, len(tree_ints)):
            if tree_ints[k][j]<tree_ints[i][j]: #Checking if tree on right is shorter than tree
                temp+=1
            else:
                temp+=1         #temp goes to 1 to prevent nullification on multiplication; we break out of the loop
                break
        vision*=temp
        temp=0

        for k in range (i-1,-1,-1):
            if tree_ints[k][j]<tree_ints[i][j]: #Checking if tree on left is shorter than tree
                temp+=1
            else:
                temp+=1     #temp goes to 1 to prevent nullification on multiplication; we break out of the loop
                break
        vision*=temp
        temp=0

        for k in range (j+1, len(tree_ints[0])):
            if tree_ints[i][k]<tree_ints[i][j]: #Checking if tree on top is shorter than tree
                temp+=1
            else:
                temp+=1         #temp goes to 1 to prevent nullification on multiplication; we break out of the loop
                break
        vision*=temp
        temp=0

        for k in range (j-1,-1,-1):
            if tree_ints[i][k]<tree_ints[i][j]: #Checking if tree on bottom is shorter than tree
                temp+=1
            else:
                temp+=1     #temp goes to 1 to prevent nullification on multiplication; we break out of the loop
                break
        vision*=temp
        if vision>highest_score:
            highest_score=vision

print ("The part 2 solution is:", highest_score)
file=open("/Users/aditilalla/programming-with-python-2022/aoc/day06.txt", "r").read()
file=list(file)
#PART 1
for i in range (4, len(file), 4):
    a=[file[i],file[i+1],file[i+2],file[i+3]]
    if (len(set(a))==4):
        print("The part 1 solution is ", i+2)  
        #we are only looking for the first instance of the marker so we break here
        break

#Part 2

#created the range of first 14 characters outside the loop for brevity (To avoid [i], [i+1]...[i+13])
file2=list(file[:14])
#Since the first range is created outside the file and we will move further one character at a time, no step needs to be programmed in the for loop range
for i in range (14, len(file)):
    #Checks if the marker is at the end of the first 14 characters
    if (len(set(file2))==14):
        print("The part 2 solution is ", i)  
        #we are only looking for the first instance of the marker so we break here
        break
    #No marker is found in the first 14 characters, so we remove the first and add the 15th and so on until we find the first marker
    file2.pop(0)
    file2.append(file[i])

#Part 1 could be rewritten in the same vein as part 2 to be cleaner but I wanted to show my initial approach to solving this problem
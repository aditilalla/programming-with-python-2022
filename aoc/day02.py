rows=[]
temp=[]
opponent=[]
self=[]
score=0

file=open("/Users/aditilalla/programming-with-python-2022/aoc/day02.txt")
for line in file:
    rows.append(line)

for i in range (0, len(rows)):
    temp=rows[i].split()
    opponent.append(temp[0])
    self.append(temp[1])

for i in range (0, len(rows)):
    if opponent[i]=='A':
        if self[i]=='X':
            score+=4
        elif self[i]=='Y':
            score+=8
        elif self[i]=='Z':
            score+=3
    elif opponent[i]=='B':
        if self[i]=='X':
            score+=1
        elif self[i]=='Y':
            score+=5
        elif self[i]=='Z':
            score+=9
    elif opponent[i]=='C':
        if self[i]=='X':
            score+=7
        elif self[i]=='Y':
            score+=2
        elif self[i]=='Z':
            score+=6

print("The part 1 score is", score)

#Part 2

score=0

for i in range (0, len(rows)):
    if opponent[i]=='A':
        if self[i]=='X':
            score+=3
        elif self[i]=='Y':
            score+=4
        elif self[i]=='Z':
            score+=8
    elif opponent[i]=='B':
        if self[i]=='X':
            score+=1
        elif self[i]=='Y':
            score+=5
        elif self[i]=='Z':
            score+=9
    elif opponent[i]=='C':
        if self[i]=='X':
            score+=2
        elif self[i]=='Y':
            score+=6
        elif self[i]=='Z':
            score+=7

print("The part 2 score is", score)
